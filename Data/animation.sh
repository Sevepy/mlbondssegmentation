#!/usr/bin/env bash

##. Requires imagemagick 
command -v convert >/dev/null 2>&1 || { echo >&2 "Require imagemagick but it's not installed"; exit 1; }

convert -delay 3000 @ImgListW.txt animated.gif
